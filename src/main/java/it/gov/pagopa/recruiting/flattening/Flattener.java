package it.gov.pagopa.recruiting.flattening;

import java.util.List;

public interface Flattener {

	/** Write some code that will flatten an array of arbitrarily nested arrays of integers into a flat array of integers. e.g. [[1,2,[3]],4] -> [1,2,3,4]. (provide a link to your solution)
	 * @param elements input integer iterable, must be not null
	 * @return flattened list
	 */
	List<Integer> flatten(Iterable<?> elements);
	
	
	/** Flattener method for input array.
	 * @see Flattener#flatten(Iterable<?>)
	 * @param elements input integer array, must be not null
	 * @return flattened array
	 */
	Integer[] flatten(Object[] elements);

}
