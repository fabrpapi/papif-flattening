package it.gov.pagopa.recruiting.flattening;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Fabrizio Papi
 * 
 */
@Slf4j
public class FlattenerImpl implements Flattener {

	@Override
	public List<Integer> flatten(Iterable<?> elements) {
		log.trace("flatten(elements={}", elements);
		List<Integer> flattened = new ArrayList<>();
		for (Object e : elements) {
			if (e == null) {
				log.debug("flattening source array contain a null element: will be discarded.");
			} else if(Integer.class.isAssignableFrom(e.getClass())) {
				log.debug("flattening integer value: {}", e);
				flattened.add((Integer)e);
			} else if(Iterable.class.isAssignableFrom(e.getClass())) {
				log.debug("flattening inner iterable {}", e);
				flattened.addAll(flatten((Iterable<?>) e));
			} else {
				log.debug("flattening not integer value {}", e);
				throw new IllegalArgumentException("can not process input element {}: flatten method only support Iterable<Integer> and Integer");
			}
		}
		log.trace("flatten return {}", flattened);
		return flattened;
	}

	@Override
	public Integer[] flatten(Object[] elements) {
		return flatten(Arrays.asList(elements)).toArray(new Integer[0]);
	}

}
