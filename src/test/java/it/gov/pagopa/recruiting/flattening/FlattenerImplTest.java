package it.gov.pagopa.recruiting.flattening;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FlattenerImplTest {

	@Test
	public void flattenSingleIntegerIterable() {
		Iterable<?> input = Arrays.asList(1);
		List<Integer> flattened = new FlattenerImpl().flatten(input);
		Assertions.assertNotNull(flattened);
		Assertions.assertEquals(1, flattened.get(0));
	}
	
	@Test
	public void flattenMultipleIntegerIterable() {
		Iterable<?> input = Arrays.asList(1,2,3);
		List<Integer> flattened = new FlattenerImpl().flatten(input);
		Assertions.assertNotNull(flattened);
		Assertions.assertAll("flattened list do not contains the same input element"
				,() -> Assertions.assertEquals(1, flattened.get(0))
				,() -> Assertions.assertEquals(2, flattened.get(1))
				,() -> Assertions.assertEquals(3, flattened.get(2))
				);
	}
	
	@Test
	public void flattenNullContainingIntegerIterable() {
		Iterable<?> input = Arrays.asList(1,null,2,3);
		List<Integer> flattened = new FlattenerImpl().flatten(input);
		Assertions.assertNotNull(flattened);
		Assertions.assertAll("flattened list do not contains the same input element"
				,() -> Assertions.assertEquals(1, flattened.get(0))
				,() -> Assertions.assertEquals(2, flattened.get(1))
				,() -> Assertions.assertEquals(3, flattened.get(2))
				);
	}
	
	@Test
	public void flattenNestedIterable() {
		Iterable<?> input = Arrays.asList(1,Arrays.asList(2,3));
		List<Integer> flattened = new FlattenerImpl().flatten(input);
		Assertions.assertNotNull(flattened);
		Assertions.assertAll("flattened list do not contains the same input element"
				,() -> Assertions.assertEquals(1, flattened.get(0))
				,() -> Assertions.assertEquals(2, flattened.get(1))
				,() -> Assertions.assertEquals(3, flattened.get(2))
				);
	}
	
	@Test
	public void flattenNotIntegerIterable() {
		Iterable<?> input = Arrays.asList(1,'a');
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			new FlattenerImpl().flatten(input);
	    });
	}
	
	@Test
	public void flattenNestedArray() {
		Object[] input = Arrays.asList(1,Arrays.asList(2,3)).toArray();
		Integer[] flattened = new FlattenerImpl().flatten(input);
		Assertions.assertNotNull(flattened);
		Assertions.assertAll("flattened list do not contains the same input element"
				,() -> Assertions.assertEquals(1, flattened [0])
				,() -> Assertions.assertEquals(2, flattened[1])
				,() -> Assertions.assertEquals(3, flattened[2])
				);
	}
	
}
